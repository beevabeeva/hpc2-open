# HPC Assignment 2 README

## Requiements
- CUDA toolkit `version 10.0` or `version 10.1`.
- NVIDIA GPU with compute capability `2.0 +`


## Makefile
Each implementation has it's own makefile.
Go into the implementations' subfolder and open a terminal from there. Type `make`.

In the Makefile:
### CUDA 10.0
Comment out the lines with `cuda-10.1`.
Uncomment the lines with `cuda-10.0`.

### CUDA 10.1
Comment out the lines with `cuda-10.0`.
Uncomment the lines with `cuda-10.1`.

## Execution

`./[implementation] data/Experiment/[image file] [convolution mask size] [convolution mask type]`

- The argument `[implementation]` can be:
	- serial
	- naive
	- constant
	- shmem
	- texture
- The argument `[convolution mask size]` must be an odd number. Try 3, 5, 11 or 15.
- The argument `[convolution mask type]` specificies the filter type:
	- 0 : Blur 
	- 1 : Sharpen
	- 2 : Edge detection (Sobel vertical)
	Note if choosing 2, the argument for `[convolution mask size]` must be 3.
### Example running the serial implementation on 3lena_bw.pgm using a blur mask of size 3:
	`./serial data/Experiment/3lena_bw.pgm 3 0`

The terminal will display the output path of the resulting image.

Output should be:

```
Serial convolution starting...
Loaded 'data/Experiment/3lena_bw.pgm', 512 x 512 pixels
...allocating GPU memory and copying input data

Wrote 'outputs/data/Experiment/3lena_bw.pgm_serial_out.pgm'
Cleaning up...
SERIAL VERSION: Processing time: 5.074000 (ms)
Overhead: 48.067997 (ms)
51.66 Mpixels/sec


```

