#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// not sure what this does?
#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

// Includes CUDA
#include <cuda_runtime.h>

// Utilities and timing functions
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h

// CUDA helper functions
#include <helper_cuda.h>         // helper functions for CUDA error check

#define MAX_EPSILON_ERROR 5e-3f


// Global variable for timer functions
StopWatchInterface *timer = NULL;
StopWatchInterface *Overhead_timer = NULL;

/// File to put experiment results into
FILE *Results;

// Define the files that are to be saved://////////////
// const char *imageFilename = "data/bird2.pgm";

const char *sampleName = "Texture Memory convolution";
/////////////////////////////////////////////////////////

// Texture reference for 2D float texture
texture<float, 2, cudaReadModeElementType> texMem;
////////////////////////////////////////////////



//////////////////////>>> Kernel Helper <<<///////////////////////////////////////////////
// Check if convolved pixel is between 0 and 1 
//  This fixes output for some of the filters like Sobel.
__device__ void fix_pix(float & pixel){
    
    if(pixel > 1.0 ){
        pixel = 1.0 ;
    } else if(pixel < 0.0 ){
        pixel = 0.0;
    }

}
/////////////////////////////////////////////////////////////////////////////////////////











///////////////##############################////////////////////////////////////////////////////////////////////////
// Kernel Function
// Texture Mem GPU implementation 
/////////////////###########################/////////////////////////////////////////////////////////////////////////
__global__ void texture_convolution(float *kernel, int kernel_size, int height, int width, float *output){

   
/// from Simple Texture
      // calculate normalized texture coordinates
      // make variables to track indicies using 2D abstraction for flattened matrix:

    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
 

    // read from texture and write to global memory
    // outputData[y*width + x] = tex2D(tex, tu+0.5f, tv+0.5f);







    //// Get the number of rows/columns that are above/below right/left the centre pixel////
    int kernel_dim = kernel_size*kernel_size;
    int half_kernel_size = kernel_size/2;
    ////////////////////////////////////////////////////////////////////////////////////////


/// thread alloc:
    int out_index = x + y*width; // to map to output array
   // printf("%d\n",blockIdx.y );
    // printf("%d\n", y);


// check bounds
    if( x < width && y < height){
        float pixel = 0;

        
        // Apply convolution to the tile
        for (int j = 0; j < kernel_dim; j++)
        {
           // get the 2D indicies of the pixel in the tile corresponding to the element in the kernel, so they may convoluted:
            int pix_X = x + ( ( (j%kernel_size) ) - half_kernel_size );
            int pix_Y = y + ( ( (j/kernel_size) ) - half_kernel_size );
                    // printf("row:%d, col:%d \n", pix_Y,pix_X);

            // Get the pixel coordinate back in 1D (mostly just to simplify the boundary check)
            int index = pix_Y*width + pix_X;

            /// Check that we are in bounds of the image - if we are not, we do nothing. 
            // NOTE: This trick won't work if the padding scheme uses values other than zero.
            if(index < width*height && index >= 0)
            {

                // Get normalised coords to fetch texture:
                float u = (pix_X + 0.5f) / (float) width; // the f after 0.5 is stuff - see the C programming language book at some point...
                float v = (pix_Y + 0.5f) / (float) height;
            // multiply corresponding element in kernel to the image pixel at index and add it to cumulative sum, i.e convolution:
                pixel += tex2D(texMem, u, v)*kernel[j];
                    // printf("%f\n", pixel);
            }
            
        }
        // Ensure the convolved pixel is within appropriate range (0 to 1):
        fix_pix(pixel);
        // Save the output:
        output[out_index] = pixel;

    }











}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





//// Start overhead timer fn _______________________
void startOverheadTimer(){
  sdkCreateTimer(&Overhead_timer);
  sdkStartTimer(&Overhead_timer);
}
//_________________________________________




// End Overhead Timer fn ________________________________________________________________________________________
void endOverheadTimer(int width, int height){
    sdkStopTimer(&Overhead_timer);
    float overhead = sdkGetTimerValue(&Overhead_timer) - sdkGetTimerValue(&timer);


    printf("TEXTURE VERSION: Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
    printf("Overhead: %f (ms)\n", overhead);
    printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
    
    Results=fopen("Texture_Results_.txt", "a");
    fprintf(Results, " %f, %.2f, %f \n", sdkGetTimerValue(&timer), (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6, overhead);
    fclose(Results);

    sdkDeleteTimer(&timer);
    sdkDeleteTimer(&Overhead_timer);

}
//_______________________________________________________________________________________________________



//// Start timer fn _______________________
void startTimer(){
  // StopWatchInterface *timer = NULL;
  checkCudaErrors(cudaDeviceSynchronize());
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);
}
//_________________________________________




// End Timer fn ________________________________________________________________________________________
void endTimer(){
    getLastCudaError("Kernel execution failed");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&timer);
}
//_______________________________________________________________________________________________________







int main(int argc, char **argv)
{
startOverheadTimer();

    printf("%s starting...\n", sampleName);

    // Handle input
    const char *imageFilename =argv[1];
    char *p;
    long kernel_size = strtol(argv[2], &p, 10);
    char *outputImagePath = "outputs/";
    int kernel_type = atoi(argv[3]); // this is better than strtol() lolol






 /////////// load image from disk //////////////////////////////////////
    float *hData = NULL;    // hData will contain the image
    unsigned int width, height;
    char *imagePath = sdkFindFilePath(imageFilename, argv[0]);

    // handle errors:
    if (imagePath == NULL)
    {
        printf("Unable to source image file: %s\n", imageFilename);
        exit(EXIT_FAILURE);
    }

    sdkLoadPGM(imagePath, &hData, &width, &height);
//////////////////////////////////////////////////////////////////////





//////// Allocate mem for the result on host side ///////
    unsigned int size = width * height * sizeof(float);
    float *hOutputData = (float *) malloc(size);    // The result from the kernel is copied here.
/////////////////////////////////////////////////////////

    // cudaDeviceSetLimit(cudaLimitMallocHeapSize, size);


    printf("Loaded '%s', %d x %d pixels\n", imageFilename, width, height);






//{{{{{{{{{{{{{{{{{{{{{{{{{{{{FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

float *kernel = (float *)malloc(kernel_size*kernel_size*sizeof(float));


if(kernel_type == 0){
////////////// Create averaging (Blur) filter kernel ///////////////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = 1.0/9.0;
    }
}else if(kernel_type == 1){
///////////////////////////////// Create sharpening filter ///////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = -1.0;
    }
    // change middle element:
    kernel[(kernel_size*kernel_size)/2] = (float)( (kernel_size*kernel_size) );
}else if(kernel_type == 2){
    
    if(kernel_type == 2 && kernel_size != 3){
    printf("ERROR: Make sure kernel size argument is 3, when using kernel type argument 2 (Sobel mask). \n");
    return 0;
    }
////////////// Create Sobel Vertical filter kernel ///////////////////////////////////
    kernel[0] = -1.0;
    kernel[1] = 0.0;
    kernel[2] = 1.0;
    kernel[3] =-2.0;
    kernel[4] = 0.0;
    kernel[5] = 2.0;
    kernel[6] = -1.0;
    kernel[7] = 0.0;
    kernel[8] = 1.0;
}
else{
    printf("ERROR: Make sure kernel type argument is between 0 and 2. \n");
    return 0;
}



//{{{{{{{{{{{{{{{{{{{{{{{END FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}






////////////////////// Allocate device mem and copy data ?? //////////////////////////
    // float *d_data;      // input data on the device (copied from hData)
    float *d_output;   // output data on device
    float *d_kernel;   // copy kernel to device mem

printf("...allocating GPU memory and copying input data\n\n");
    // checkCudaErrors(cudaMalloc((void **)&d_data, size));
    checkCudaErrors(cudaMalloc((void **)&d_output, size ) );
    checkCudaErrors(cudaMalloc((void **)&d_kernel, kernel_size*kernel_size*sizeof(float)) );
    // checkCudaErrors(cudaMemcpy(hData, size, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_kernel, kernel, kernel_size*kernel_size*sizeof(float), cudaMemcpyHostToDevice) );



//////////////////////////// Texture Alloc/////////////////////////////////////////

     // Allocate array and copy image data
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    cudaArray *cuArray;
    checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, width, height));
    // copy image into cuArray
    checkCudaErrors(cudaMemcpyToArray(cuArray, 0, 0, hData, size, cudaMemcpyHostToDevice) );

    // Set texture parameters
    texMem.addressMode[0] = cudaAddressModeWrap;//?????
    texMem.addressMode[1] = cudaAddressModeWrap;//?????
    texMem.filterMode = cudaFilterModeLinear; // doesn't do interpolation
    texMem.normalized = true;                 // access with normalized texture coordinates

    // Bind the array to the texture
    checkCudaErrors(cudaBindTextureToArray(texMem, cuArray, channelDesc) );

    


/////////////////////////////////////////////////////////////////////////


// define kernel config
    int blocksize = height*width;
    dim3 blk_size(32, 32); //2D block - maxed out with 32, since 32^2 =1024 = max threads for current arch.
    dim3 grid_size(ceil((float)width /(float)blk_size.x), ceil((float)height / (float)blk_size.y) ); // 2D grid


startTimer();
// call kernel////////////////////////////////////////////////////////////////////////////////////////
    texture_convolution<<<grid_size, blk_size>>>(d_kernel, kernel_size, height, width, d_output);
///////////////////////////////////////////////////////////////////////////////////////////////////////
endTimer();

    // cudaDeviceSynchronize();

// verify if exec'd
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(err));






// copy result to host
checkCudaErrors(cudaMemcpy(hOutputData, d_output, size, cudaMemcpyDeviceToHost));






//////////////////////////////////////////////////////////////////////////////////////






/// Save output image to file /////////////////////////////////////////////////////
    char outputFilename[1024];
    strcpy(outputFilename, outputImagePath);    
    strcpy(outputFilename + strlen(outputImagePath), imageFilename );
    strcat(outputFilename , "_texture_out.pgm");
    sdkSavePGM(outputFilename, hOutputData, width, height);
    printf("Wrote '%s'\n", outputFilename);
///////////////////////////////////////////////////////////////////////////////////







    printf("Cleaning up...\n");

    checkCudaErrors(cudaFree(d_output));
    checkCudaErrors(cudaFreeArray(cuArray));
    
    free(hData);
    free(hOutputData);


    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    cudaDeviceReset();








endOverheadTimer(width, height);
}
