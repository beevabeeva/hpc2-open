#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// not sure what this does?
#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

// Includes CUDA
#include <cuda_runtime.h>

// Utilities and timing functions
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h

// CUDA helper functions
#include <helper_cuda.h>         // helper functions for CUDA error check

#define MAX_EPSILON_ERROR 5e-3f


// Define the files that are to be saved:
const char *imageFilename = "lena_bw.pgm";

const char *sampleName = "Serial convolution";


///////////////##############################///////////////////////////////////
// Kernel Function
// Nothing in kernel for serial versions
/////////////////###########################/////////////////////////////////////



void convolution(){

}


int main(int argc, char **argv)
{
    printf("%s starting...\n", sampleName);

    
}
