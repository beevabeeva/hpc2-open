#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// not sure what this does?
#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

// Includes CUDA
#include <cuda_runtime.h>

// Utilities and timing functions
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h

// CUDA helper functions
#include <helper_cuda.h>         // helper functions for CUDA error check
# include <cuda_profiler_api.h>

#define MAX_EPSILON_ERROR 5e-3f


// Define the files that are to be saved:
// const char *imageFilename = "data/lena_bw.pgm";
// const char *imageFilename = "data/bird2.pgm";


const char *sampleName = "Naive convolution";

/// File to put experiment results into
FILE *Results;

// Global variable for timer forunctions
StopWatchInterface *timer = NULL;
StopWatchInterface *Overhead_timer = NULL;


//////////////////////>>> Kernel Helper <<<///////////////////////////////////////////////
// Check if convolved pixel is between 0 and 1 
//  This fixes output for some of the filters like Sobel.
__device__ void fix_pix(float & pixel){
    
    if(pixel > 1.0 ){
        pixel = 1.0 ;
    } else if(pixel < 0.0 ){
        pixel = 0.0;
    }

}
/////////////////////////////////////////////////////////////////////////////////////////












///////////////##############################////////////////////////////////////////////////////////////////////////
// Kernel Function
// Naive GPU implementation 
/////////////////###########################/////////////////////////////////////////////////////////////////////////
__global__ void naive_convolution(float *image, float *kernel, int kernel_size, int height, int width, float *output){

    //// Get the number of rows/columns that are above/below right/left the centre pixel////
    int kernel_dim = kernel_size*kernel_size;
    int half_kernel_size = kernel_size/2;
    ////////////////////////////////////////////////////////////////////////////////////////


/// thread alloc:
    int y=threadIdx.x + blockIdx.x * blockDim.x;
   // printf("%d\n",blockIdx.y );
    // printf("%d\n", y);


// check bounds
    if(y< height*width){
        float pixel = 0;

        // make variables to track indicies using 2D abstraction for flattened matrix:
        int row = y/width;
        int col = y -(row *width);

        // Apply convolution to the tile
        for (int j = 0; j < kernel_dim; j++)
        {
           // get the 2D indicies of the pixel in the tile corresponding to the element in the kernel, so they may convoluted:
            int pix_X = col + ( ( (j%kernel_size) ) - half_kernel_size );
            int pix_Y = row + ( ( (j/kernel_size) ) - half_kernel_size );
                    // printf("row:%d, col:%d \n", pix_Y,pix_X);

            // Get the pixel coordinate back in 1D:
            int index = pix_Y*width + pix_X;

            /// Check that we are in bounds of the image - if we are not, we do nothing. 
            // NOTE: This trick won't work if the padding scheme uses values other than zero.
            if(index < width*height && index >= 0)
            {
            // multiply corresponding element in kernel to the image pixel at index and add it to cumulative sum, i.e convolution:
                pixel += image[index]*kernel[j];
                    // printf("%f\n", pixel);
            }
            
        }
        // Ensure the convolved pixel is within appropriate range (0 to 1):
        fix_pix(pixel);
        // Save the output:
        output[y] = pixel;

    }











}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







///////////////##############################/////////////////////////////////////////////////////////////
// Serial Convolution Function
// 
/////////////////###########################/////////////////////////////////////////////////////////////

void convolution(float *image, float *kernel, int kernel_size, int height, int width, float *output){



//// Get the number of rows/columns that are above/below right/left the centre pixel////
    int kernel_dim = kernel_size*kernel_size;
    int half_kernel_size = kernel_size/2;
////////////////////////////////////////////////////////////////////////////////////////

    
// Note the image from SDK function is flattened:

	for (int y = 0; y < height*width; y++)
	{
       float pixel = 0;

        // make variables to track indicies using 2D abstraction for flattened matrix:
        int row = y/width;
        int col = y -(row *width);

        // Apply convolution to the tile
        for (int j = 0; j < kernel_dim; j++)
        {
           // get the 2D indicies of the pixel in the tile corresponding to the element in the kernel, so they may convoluted:
            int pix_X = col + ( ( (j%kernel_size) ) - half_kernel_size );
            int pix_Y = row + ( ( (j/kernel_size) ) - half_kernel_size );
                    // printf("row:%d, col:%d \n", pix_Y,pix_X);

            // Get the pixel coordinate back in 1D:
            int index = pix_Y*width + pix_X;

            /// Check that we are in bounds of the image - if we are not, we do nothing. 
            // NOTE: This trick won't work if the padding scheme uses values other than zero.
            if(index < width*height && index >= 0)
            {
            // multiply corresponding element in kernel to the image pixel at index and add it to cumulative sum, i.e convolution:
                pixel += image[index]*kernel[j];
                    // printf("%f\n", pixel);
            }
            
        }

        // Save the output:
		output[y] = pixel;
	}

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//// Start overhead timer fn _______________________
void startOverheadTimer(){
  // StopWatchInterface *timer = NULL;
  // checkCudaErrors(cudaDeviceSynchronize());
  sdkCreateTimer(&Overhead_timer);
  sdkStartTimer(&Overhead_timer);
}
//_________________________________________




// End Overhead Timer fn ________________________________________________________________________________________
void endOverheadTimer(int width, int height){
    // getLastCudaError("Kernel execution failed");
    // checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&Overhead_timer);
    float overhead = sdkGetTimerValue(&Overhead_timer) - sdkGetTimerValue(&timer);


    printf("NAIVE VERSION: Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
    printf("Overhead: %f (ms)\n", overhead);
    printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
    
    Results=fopen("Naive_Results_.txt", "a");
    fprintf(Results, " %f, %.2f, %f \n", sdkGetTimerValue(&timer), (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6, overhead);
    fclose(Results);

    sdkDeleteTimer(&timer);
    sdkDeleteTimer(&Overhead_timer);

}
//_______________________________________________________________________________________________________



//// Start timer fn _______________________
void startTimer(){
  // StopWatchInterface *timer = NULL;
  checkCudaErrors(cudaDeviceSynchronize());
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);
}
//_________________________________________




// End Timer fn ________________________________________________________________________________________
void endTimer(){
    getLastCudaError("Kernel execution failed");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&timer);
    // printf("SERIAL VERSION: Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
    // printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
    // Results=fopen("Serial_Results_9x9_Test2.txt", "a");
    // fprintf(Results, " %f, %.2f \n", sdkGetTimerValue(&timer), (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
    // fclose(Results);
    //sdkDeleteTimer(&timer);

}
//_______________________________________________________________________________________________________












int main(int argc, char **argv)
{
    cudaProfilerStart();

startOverheadTimer();

    printf("%s starting...\n", sampleName);



// Handle input
    const char *imageFilename =argv[1];
    char *p;
    long kernel_size = strtol(argv[2], &p, 10);
    char *outputImagePath = "outputs/";
    int kernel_type = atoi(argv[3]); // this is better than strtol() lolol




 /////////// load image from disk //////////////////////////////////////
    float *hData = NULL;    // hData will contain the image
    unsigned int width, height;
    char *imagePath = sdkFindFilePath(imageFilename, argv[0]);

    // handle errors:
    if (imagePath == NULL)
    {
        printf("Unable to source image file: %s\n", imageFilename);
        exit(EXIT_FAILURE);
    }

    sdkLoadPGM(imagePath, &hData, &width, &height);
//////////////////////////////////////////////////////////////////////





//////// Allocate mem for the result on host side ///////
    unsigned int size = width * height * sizeof(float);
    float *hOutputData = (float *) malloc(size);    // The result from the kernel is copied here.
/////////////////////////////////////////////////////////

    // cudaDeviceSetLimit(cudaLimitMallocHeapSize, size);


    printf("Loaded '%s', %d x %d pixels\n", imageFilename, width, height);
   




//{{{{{{{{{{{{{{{{{{{{{{{{{{{{FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

float *kernel = (float *)malloc(kernel_size*kernel_size*sizeof(float));


if(kernel_type == 0){
////////////// Create averaging (Blur) filter kernel ///////////////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = 1.0/9.0;
    }
}else if(kernel_type == 1){
///////////////////////////////// Create sharpening filter ///////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = -1.0;
    }
    // change middle element:
    kernel[(kernel_size*kernel_size)/2] = (float)( (kernel_size*kernel_size) );
}else if(kernel_type == 2){
    
    if(kernel_type == 2 && kernel_size != 3){
    printf("ERROR: Make sure kernel size argument is 3, when using kernel type argument 2 (Sobel mask). \n");
    return 0;
    }
////////////// Create Sobel Vertical filter kernel ///////////////////////////////////
    kernel[0] = -1.0;
    kernel[1] = 0.0;
    kernel[2] = 1.0;
    kernel[3] =-2.0;
    kernel[4] = 0.0;
    kernel[5] = 2.0;
    kernel[6] = -1.0;
    kernel[7] = 0.0;
    kernel[8] = 1.0;
}
else{
    printf("ERROR: Make sure kernel type argument is between 0 and 2. \n");
    return 0;
}



//{{{{{{{{{{{{{{{{{{{{{{{END FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}




////////////////////// Allocate device mem and copy data ?? //////////////////////////
    float *d_data;      // input data on the device (copied from hData)
    float *d_output;   // output data on device
    float *d_kernel;   // copy kernel to device mem

printf("...allocating GPU memory and copying input data\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_data, size));
    checkCudaErrors(cudaMalloc((void **)&d_output, size ) );
    checkCudaErrors(cudaMalloc((void **)&d_kernel, kernel_size*kernel_size*sizeof(float)) );
    checkCudaErrors(cudaMemcpy(d_data, hData, size, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_kernel, kernel, kernel_size*kernel_size*sizeof(float), cudaMemcpyHostToDevice) );


// define kernel config
    int blocksize = height*width;
    dim3 blk_size(1024);
    dim3 grid_size(ceil(height*width/1024.0));




startTimer();
// call kernel //////////////////////////////////////////////////////////////////////////////////////////
    naive_convolution<<<grid_size, blk_size>>>(d_data, d_kernel, kernel_size, height, width, d_output);
//////////////////////////////////////////////////////////////////////////////////////////////////////////
endTimer(); 


// cudaDeviceSynchronize();

// verify if exec'd
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(err));






// copy result to host
checkCudaErrors(cudaMemcpy(hOutputData, d_output, size, cudaMemcpyDeviceToHost));






//////////////////////////////////////////////////////////////////////////////////////






/// Save output image to file /////////////////////////////////////////////////////
    char outputFilename[1024];
    strcpy(outputFilename, outputImagePath);    
    strcpy(outputFilename + strlen(outputImagePath), imageFilename );
    strcat(outputFilename , "_naive_out.pgm");
    sdkSavePGM(outputFilename, hOutputData, width, height);
    printf("Wrote '%s'\n", outputFilename);
///////////////////////////////////////////////////////////////////////////////////







    printf("Cleaning up...\n");

    checkCudaErrors(cudaFree(d_data));
    checkCudaErrors(cudaFree(d_output));

    free(hData);
    free(hOutputData);

    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    cudaDeviceReset();







endOverheadTimer(width, height);
cudaProfilerStop();


}
