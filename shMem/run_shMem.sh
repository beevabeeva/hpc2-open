#!/bin/bash

# based on answers from :
# https://stackoverflow.com/questions/8512462/shellscript-looping-through-all-files-in-a-folder
# https://stackoverflow.com/questions/16034749/if-elif-else-statement-issues-in-bash

j=15
# loop through images:
for filename in data/Experiment/*; 
# do echo "${filename}"; 
do
	echo "${filename}" >> shMem_Results_15.txt
# loop through kernel types (only sharp and blurr)
	for i in {0..1};
	do
		# kernel type
		echo "$i" >> shMem_Results_15.txt
# loop through kernel size
		# if [ $i -eq 2 ]
		# then
# if sobel filter then only do kernel size 3	 
			# kernel size
			# echo "3" >> shMem_Results_15.txt
	# do it 10 times
			# for k in {1..10};
			# do
			# 	./shmem "${filename}" 3 $i
			# 	# echo "Welcome $i times"
			# done
		# else
			# for j in {3,9,11,15};
			# do
				# kernel size
				echo "$j" >> shMem_Results_15.txt
	# do it 10 times
				for k in {1..10};
				do
					./shmem "${filename}" $j $i
					# echo "Welcome $i times"
				done
			# done
		# fi
	done
	echo $'\n' >> shMem_Results_15.txt
done
