#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// not sure what this does?
#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

// Includes CUDA
#include <cuda_runtime.h>

// Utilities and timing functions
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h

// CUDA helper functions
#include <helper_cuda.h>         // helper functions for CUDA error check

#define MAX_EPSILON_ERROR 5e-3f


///////////////////////////////////////////
// Define the files that are to be saved:
// const char *imageFilename = "data/compare/lena_bw.pgm";
// const char *imageFilename = "data/bird2.pgm";

const char *sampleName = "Shared Memory convolution";
////////////////////////////////////////////


#define BLOCK_WIDTH 32
#define kernelWidth 3
#define TILE_WIDTH (BLOCK_WIDTH + kernelWidth -1)

// Global variable for timer functions
StopWatchInterface *timer = NULL;
StopWatchInterface *Overhead_timer = NULL;



/// File to put experiment results into
FILE *Results;

//////////////////////>>> Kernel Helper <<<///////////////////////////////////////////////
// Check if convolved pixel is between 0 and 1 
//  This fixes output for some of the filters like Sobel.
//  It is normal for convolution may yield values less than zero and more than one

__device__ void fix_pix(float & pixel){
    
    if(pixel > 1.0 ){
        pixel = 1.0 ;
    } else if(pixel < 0.0 ){
        pixel = 0.0;
    }

}
/////////////////////////////////////////////////////////////////////////////////////////












///////////////##############################////////////////////////////////////////////////////////////////////////
// Kernel Function
// Shared Memory implementation 
/////////////////###########################/////////////////////////////////////////////////////////////////////////
__global__ void shMem_convolution(float *image, float *kernel, int kernel_size, int height, int width, float *output){

//// Get the number of rows/columns that are above/below right/left the centre pixel////
int kernel_dim = kernel_size*kernel_size;
int half_kernel_size = kernel_size/2;
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////// SHMEM ////////////////////////////////////////////////////////////
    __shared__ float shmem[TILE_WIDTH][TILE_WIDTH];
////////////////////////////////////////////////////////////////////////////////////////////





/////////////////////// thread alloc:///////////////////////////////////////////////////////////TTTTT

    // from lecture slides6_updated (Tiled matrix example):
    int bx = blockIdx.x; 
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;
// // Identify the row and column of the d_P element to work on
    int Row = by * blockDim.y + ty;
    int Col = bx * blockDim.x + tx;
    int img_pixel = Col + Row*width; // 1D coordinate of pixel in terms of the image

    int tile_pixel = tx + ty*BLOCK_WIDTH; // 1D coordinate of the pixel in terms of the tile (Recall the tile is larger than the subimage -it has padding)
 //////////// Mappings ///////////////////////
    int twoD_tile_x, twoD_tile_y, twoD_pad_x, twoD_pad_y, oneD_pad;



    // Loop over the cells in a shmem tile and allocate elements from original image but for each pixel, take it's neighbours
    int stride = BLOCK_WIDTH*BLOCK_WIDTH; //TODO: Play with this - find theoretical way to choose it
    for( int ph = tile_pixel; ph < TILE_WIDTH*TILE_WIDTH; ph += stride){

        // Map block index of pixel to 2D:

        twoD_tile_y = ph/TILE_WIDTH; // gives row
        twoD_tile_x = ph - twoD_tile_y*TILE_WIDTH; //gives col

       

        // Map the current pixel's padding neighbours to 2D in terms of the image corodinates:

        twoD_pad_y = twoD_tile_y + by*blockDim.y - half_kernel_size;
        twoD_pad_x = twoD_tile_x + bx*blockDim.x - half_kernel_size;


        // Map padding to 1D:
        oneD_pad = twoD_pad_x + twoD_pad_y*width;



        if( oneD_pad < width*height && oneD_pad >= 0){
            shmem[twoD_tile_y][twoD_tile_x] = image[oneD_pad];
        } else{
            shmem[twoD_tile_y][twoD_tile_x] = 0.0;
        }
    }
    __syncthreads();



//////////////////////////////////////////////////////////////////////////////////////////////////TTT


// check bounds
    if(img_pixel < height*width){
        float pixel = 0;
        tx = tx + half_kernel_size;
        ty = ty + half_kernel_size;
        int k_index = 0;

        // make variables to track indicies using 2D abstraction for flattened matrix:
        // int row = y/width;
        // int col = y -(row *width);

        // Apply convolution to the tile
        // for (int j = 0; j < kernel_dim; j++)
        // {
            for(int i = ty - half_kernel_size; i < ty + half_kernel_size + 1; i++)
            {
                for(int j = tx - half_kernel_size; j < tx + half_kernel_size +1; j++){
           // get the 2D indicies of the pixel in the tile corresponding to the element in the kernel, so they may convoluted:
            // int pix_X = tx + ( ( (j%kernel_size) ) - half_kernel_size );
            // int pix_Y = ty + ( ( (j/kernel_size) ) - half_kernel_size );
                    // printf("row:%d, col:%d \n", pix_Y,pix_X);

            // Get the pixel coordinate back in 1D:
            // int index = pix_Y*width + pix_X;
                    // int index = i*width + j;


            /// Check that we are in bounds of the image - if we are not, we do nothing. 
            // NOTE: This trick won't work if the padding scheme uses values other than zero.
            // if(index < width*height  && index >= 0)
            // {
            // multiply corresponding element in kernel to the image pixel at index and add it to cumulative sum, i.e convolution:
                // pixel += image[index]*kernel[j];
                // pixel += shmem[pix_Y][pix_X]*kernel[j];
                pixel += shmem[i][j]*kernel[k_index];
                k_index++;
                    // printf("%f\n", pixel);

            // }
            }

        }
    //     // Ensure the convolved pixel is within appropriate range (0 to 1):
        fix_pix(pixel);
        // Save the output:
        // output[y] = shmem[ty][tx];
        output[img_pixel] = pixel;
    }











}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







//// Start overhead timer fn _______________________
void startOverheadTimer(){
  sdkCreateTimer(&Overhead_timer);
  sdkStartTimer(&Overhead_timer);
}
//_________________________________________




// End Overhead Timer fn ________________________________________________________________________________________
void endOverheadTimer(int width, int height){
    sdkStopTimer(&Overhead_timer);
    float overhead = sdkGetTimerValue(&Overhead_timer) - sdkGetTimerValue(&timer);


    printf("SHARED MEMORY VERSION: Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
    printf("Overhead: %f (ms)\n", overhead);
    printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
    
    Results=fopen("shMem_Results_15.txt", "a");
    fprintf(Results, " %f, %.2f, %f \n", sdkGetTimerValue(&timer), (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6, overhead);
    fclose(Results);

    sdkDeleteTimer(&timer);
    sdkDeleteTimer(&Overhead_timer);

}
//_______________________________________________________________________________________________________



//// Start timer fn _______________________
void startTimer(){
  // StopWatchInterface *timer = NULL;
  checkCudaErrors(cudaDeviceSynchronize());
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);
}
//_________________________________________




// End Timer fn ________________________________________________________________________________________
void endTimer(){
    getLastCudaError("Kernel execution failed");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&timer);
}
//_______________________________________________________________________________________________________

















int main(int argc, char **argv)
{
startOverheadTimer();
    printf("%s starting...\n", sampleName);


// findCudaDevice(argc, (const char **) argv);

// Handle input:
    const char *imageFilename =argv[1];
    char *p;
    int kernel_size = kernelWidth; //unfortunately can't be input since its required to be constant and I dont feel like messing with dynamic shared mem
    char *outputImagePath = "outputs/";
    int kernel_type = atoi(argv[3]); // this is better than strtol() lolol



 /////////// load image from disk //////////////////////////////////////
    float *hData = NULL;    // hData will contain the image
    unsigned int width, height;
    char *imagePath = sdkFindFilePath(imageFilename, argv[0]);

    // handle errors:
    if (imagePath == NULL)
    {
        printf("Unable to source image file: %s\n", imageFilename);
        exit(EXIT_FAILURE);
    }

    sdkLoadPGM(imagePath, &hData, &width, &height);
//////////////////////////////////////////////////////////////////////





//////// Allocate mem for the result on host side ///////
    unsigned int size = width * height * sizeof(float);
    float *hOutputData = (float *) malloc(size);    // The result from the kernel is copied here.
/////////////////////////////////////////////////////////

    // cudaDeviceSetLimit(cudaLimitMallocHeapSize, size);


    printf("Loaded '%s', %d x %d pixels\n", imageFilename, width, height);
   





//{{{{{{{{{{{{{{{{{{{{{{{{{{{{FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

float *kernel = (float *)malloc(kernel_size*kernel_size*sizeof(float));


if(kernel_type == 0){
////////////// Create averaging (Blur) filter kernel ///////////////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = 1.0/9.0;
    }
}else if(kernel_type == 1){
///////////////////////////////// Create sharpening filter ///////////////////////////
    for (int ii = 0; ii < kernel_size*kernel_size; ii++) {
      kernel[ii] = -1.0;
    }
    // change middle element:
    kernel[(kernel_size*kernel_size)/2] = (float)( (kernel_size*kernel_size) );
}else if(kernel_type == 2){
    
    if(kernel_type == 2 && kernel_size != 3){
    printf("ERROR: Make sure kernel size argument is 3, when using kernel type argument 2 (Sobel mask). \n");
    printf("%d\n",kernel_size );
    return 0;
    }
////////////// Create Sobel Vertical filter kernel ///////////////////////////////////
    kernel[0] = -1.0;
    kernel[1] = 0.0;
    kernel[2] = 1.0;
    kernel[3] =-2.0;
    kernel[4] = 0.0;
    kernel[5] = 2.0;
    kernel[6] = -1.0;
    kernel[7] = 0.0;
    kernel[8] = 1.0;
}
else{
    printf("ERROR: Make sure kernel type argument is between 0 and 2. \n");
    return 0;
}



//{{{{{{{{{{{{{{{{{{{{{{{END FILTERS}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}






////////////////////// Allocate device mem and copy data ?? //////////////////////////
    float *d_data;      // input data on the device (copied from hData)
    float *d_output;   // output data on device
    float *d_kernel;   // copy kernel to device mem

printf("...allocating GPU memory and copying input data\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_data, size));
    checkCudaErrors(cudaMalloc((void **)&d_output, size ) );
    checkCudaErrors(cudaMalloc((void **)&d_kernel, kernel_size*kernel_size*sizeof(float)) );
    checkCudaErrors(cudaMemcpy(d_data, hData, size, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_kernel, kernel, kernel_size*kernel_size*sizeof(float), cudaMemcpyHostToDevice) );


///////////////////////////// kERNEL CONFIG ///////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// See this for source of iDivUp: https://books.google.co.za/books?id=zpHNBQAAQBAJ&pg=PA103&lpg=PA103&dq=iDivUp&source=bl&ots=HqctjVRQiv&sig=ACfU3U3-l48mlpLWhjNwpkjvx2tEurEhkA&hl=en&sa=X&ved=2ahUKEwiFmLX_p8PhAhU0RRUIHUB8AAYQ6AEwBXoECAcQAQ#v=onepage&q=iDivUp&f=false
    int blocksize = height*width;
    int numBlocks_width=ceil(width/BLOCK_WIDTH);
    int numBlocks_height=ceil(height/BLOCK_WIDTH);

    if(width % BLOCK_WIDTH) numBlocks_width++;
    if(height % BLOCK_WIDTH) numBlocks_height++;
    dim3 blk_size(BLOCK_WIDTH, BLOCK_WIDTH);
    dim3 grid_size(numBlocks_width, numBlocks_height);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////






startTimer();

// call kernel
    shMem_convolution<<<grid_size, blk_size>>>(d_data, d_kernel, kernel_size, height, width, d_output);

endTimer();





// verify if exec'd
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(err));






// copy result to host
checkCudaErrors(cudaMemcpy(hOutputData, d_output, size, cudaMemcpyDeviceToHost));






//////////////////////////////////////////////////////////////////////////////////////






/// Save output image to file /////////////////////////////////////////////////////
    char outputFilename[1024];
    strcpy(outputFilename, outputImagePath);    
    strcpy(outputFilename + strlen(outputImagePath), imageFilename );
    strcat(outputFilename , "_shMem_out.pgm");
    sdkSavePGM(outputFilename, hOutputData, width, height);
    printf("Wrote '%s'\n", outputFilename);
///////////////////////////////////////////////////////////////////////////////////







    printf("Cleaning up...\n");

    checkCudaErrors(cudaFree(d_data));
    checkCudaErrors(cudaFree(d_output));
    checkCudaErrors(cudaFree(d_kernel));


    free(hData);
    free(hOutputData);

    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    cudaDeviceReset();








endOverheadTimer(width, height);
}
