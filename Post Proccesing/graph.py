import numpy as np
import matplotlib.pyplot as plt
import re

x_images = [65536, 225000, 262144, 414720, 534315, 794808, 1048576, 2105585, 4320000, 6497280]
x_filter = [3, 9, 11, 15]


################# NAIVE ######################

f = open("Naive_Results_double forloop.txt", "r")

lines = f.readlines()

n = len(lines)

naive_run_blur = np.zeros((10,4))
naive_run_sharp = np.zeros((10,4))
naive_run_edge = np.zeros((10,1))

naive_mpixel_blur = np.zeros((10,4))
naive_mpixel_sharp = np.zeros((10,4))
naive_mpixel_edge = np.zeros((10,1))

naive_overhead_blur = np.zeros((10,4))
naive_overhead_sharp = np.zeros((10,4))
naive_overhead_edge = np.zeros((10,1))

# for j in range(105):
# 	print(lines[j])

for image in range(10):
	for ftype in range(3):
		if (ftype == 2):
			filter_range = 1;
		else:
			filter_range = 4;

		for fsize in range(filter_range):

			i = 2 + ftype*45 + fsize*11 + image*105
			# print(lines[i])
			ave0 = 0.0;
			ave1 = 0.0
			ave2 = 0.0;

			for j in range(10):
				l = lines[i + j + 1]
				t = re.sub(r"\s+", "", l)
				x = t.split(",")
				# print(x[0])
				ave0 = ave0 + float(x[0])
				ave1 = ave1 + float(x[1])
				ave2 = ave2 + float(x[2])
				
			if ftype == 0:
				naive_run_blur[image, fsize] = ave0/10
				naive_mpixel_blur[image, fsize] = ave1/10
				naive_overhead_blur[image, fsize] = ave2/10
			elif ftype == 1:
				naive_run_sharp[image, fsize] = ave0/10
				naive_mpixel_sharp[image, fsize] = ave1/10
				naive_overhead_sharp[image, fsize] = ave2/10
			elif ftype == 2:
				naive_run_edge[image, fsize] = ave0/10
				naive_mpixel_edge[image, fsize] = ave1/10
				naive_overhead_edge[image, fsize] = ave2/10


		
# print(naive_run_blur)
# print(naive_run_sharp)
# print(naive_run_edge)

# print(naive_mpixel_blur)
# print(naive_mpixel_sharp)
# print(naive_mpixel_edge)

# print(naive_overhead_blur)
# print(naive_overhead_sharp)
# print(naive_overhead_edge)


y_naive = naive_run_blur[:,0] # 3x3 blurring filter
plt.plot (x_images, y_naive, label = 'Naive')

#######################################################


################# SERIAL ######################

f = open("Serial_Results_.txt", "r")

lines = f.readlines()

n = len(lines)

serial_run_blur = np.zeros((10,4))
serial_run_sharp = np.zeros((10,4))
serial_run_edge = np.zeros((10,1))

serial_mpixel_blur = np.zeros((10,4))
serial_mpixel_sharp = np.zeros((10,4))
serial_mpixel_edge = np.zeros((10,1))

serial_overhead_blur = np.zeros((10,4))
serial_overhead_sharp = np.zeros((10,4))
serial_overhead_edge = np.zeros((10,1))

# for j in range(105):
# 	print(lines[j])

for image in range(10):
	for ftype in range(3):
		if (ftype == 2):
			filter_range = 1;
		else:
			filter_range = 4;

		for fsize in range(filter_range):

			i = 2 + ftype*45 + fsize*11 + image*105
			# print(lines[i])
			ave0 = 0.0;
			ave1 = 0.0
			ave2 = 0.0;

			for j in range(10):
				l = lines[i + j + 1]
				t = re.sub(r"\s+", "", l)
				x = t.split(",")
				# print(x[0])
				ave0 = ave0 + float(x[0])
				ave1 = ave1 + float(x[1])
				ave2 = ave2 + float(x[2])
				
			if ftype == 0:
				serial_run_blur[image, fsize] = ave0/10
				serial_mpixel_blur[image, fsize] = ave1/10
				serial_overhead_blur[image, fsize] = ave2/10
			elif ftype == 1:
				serial_run_sharp[image, fsize] = ave0/10
				serial_mpixel_sharp[image, fsize] = ave1/10
				serial_overhead_sharp[image, fsize] = ave2/10
			elif ftype == 2:
				serial_run_edge[image, fsize] = ave0/10
				serial_mpixel_edge[image, fsize] = ave1/10
				serial_overhead_edge[image, fsize] = ave2/10


		
# print(serial_run_blur)
# print(serial_run_sharp)
# print(serial_run_edge)

# print(serial_mpixel_blur)
# print(serial_mpixel_sharp)
# print(serial_mpixel_edge)

# print(serial_overhead_blur)
# print(serial_overhead_sharp)
# print(serial_overhead_edge)


# y_serial = serial_run_blur[:,0] # 3x3 blurring filter
# plt.plot (x_images, y_serial, label = 'Serial')

#######################################################

################# constant ######################

f = open("Constant_Results_Double_forloop.txt", "r")

lines = f.readlines()

n = len(lines)

constant_run_blur = np.zeros((10,4))
constant_run_sharp = np.zeros((10,4))
constant_run_edge = np.zeros((10,1))

constant_mpixel_blur = np.zeros((10,4))
constant_mpixel_sharp = np.zeros((10,4))
constant_mpixel_edge = np.zeros((10,1))

constant_overhead_blur = np.zeros((10,4))
constant_overhead_sharp = np.zeros((10,4))
constant_overhead_edge = np.zeros((10,1))

# for j in range(105):
# 	print(lines[j])

for image in range(10):
	for ftype in range(3):
		if (ftype == 2):
			filter_range = 1;
		else:
			filter_range = 4;

		for fsize in range(filter_range):

			i = 2 + ftype*45 + fsize*11 + image*105
			# print(lines[i])
			ave0 = 0.0;
			ave1 = 0.0
			ave2 = 0.0;

			for j in range(10):
				l = lines[i + j + 1]
				t = re.sub(r"\s+", "", l)
				x = t.split(",")
				# print(x[0])
				ave0 = ave0 + float(x[0])
				ave1 = ave1 + float(x[1])
				ave2 = ave2 + float(x[2])
				
			if ftype == 0:
				constant_run_blur[image, fsize] = ave0/10
				constant_mpixel_blur[image, fsize] = ave1/10
				constant_overhead_blur[image, fsize] = ave2/10
			elif ftype == 1:
				constant_run_sharp[image, fsize] = ave0/10
				constant_mpixel_sharp[image, fsize] = ave1/10
				constant_overhead_sharp[image, fsize] = ave2/10
			elif ftype == 2:
				constant_run_edge[image, fsize] = ave0/10
				constant_mpixel_edge[image, fsize] = ave1/10
				constant_overhead_edge[image, fsize] = ave2/10


		
# print(constant_run_blur)
# print(constant_run_sharp)
# print(constant_run_edge)

# print(constant_mpixel_blur)
# print(constant_mpixel_sharp)
# print(constant_mpixel_edge)

# print(constant_overhead_blur)
# print(constant_overhead_sharp)
# print(constant_overhead_edge)


y_constant = constant_run_blur[:,0] # 3x3 blurring filter
plt.plot (x_images, y_constant, label = 'Constant Memory')

#######################################################

################# texture ######################

f = open("Texture_Results_Double_For_Loop.txt", "r")

lines = f.readlines()

n = len(lines)

texture_run_blur = np.zeros((10,4))
texture_run_sharp = np.zeros((10,4))
texture_run_edge = np.zeros((10,1))

texture_mpixel_blur = np.zeros((10,4))
texture_mpixel_sharp = np.zeros((10,4))
texture_mpixel_edge = np.zeros((10,1))

texture_overhead_blur = np.zeros((10,4))
texture_overhead_sharp = np.zeros((10,4))
texture_overhead_edge = np.zeros((10,1))

# for j in range(105):
# 	print(lines[j])

for image in range(10):
	for ftype in range(3):
		if (ftype == 2):
			filter_range = 1;
		else:
			filter_range = 4;

		for fsize in range(filter_range):

			i = 2 + ftype*45 + fsize*11 + image*105
			# print(lines[i])
			ave0 = 0.0;
			ave1 = 0.0
			ave2 = 0.0;

			for j in range(10):
				l = lines[i + j + 1]
				t = re.sub(r"\s+", "", l)
				x = t.split(",")
				# print(x[0])
				ave0 = ave0 + float(x[0])
				ave1 = ave1 + float(x[1])
				ave2 = ave2 + float(x[2])
				
			if ftype == 0:
				texture_run_blur[image, fsize] = ave0/10
				texture_mpixel_blur[image, fsize] = ave1/10
				texture_overhead_blur[image, fsize] = ave2/10
			elif ftype == 1:
				texture_run_sharp[image, fsize] = ave0/10
				texture_mpixel_sharp[image, fsize] = ave1/10
				texture_overhead_sharp[image, fsize] = ave2/10
			elif ftype == 2:
				texture_run_edge[image, fsize] = ave0/10
				texture_mpixel_edge[image, fsize] = ave1/10
				texture_overhead_edge[image, fsize] = ave2/10


		
# print(texture_run_blur)
# print(texture_run_sharp)
# print(texture_run_edge)

# print(texture_mpixel_blur)
# print(texture_mpixel_sharp)
# print(texture_mpixel_edge)

# print(texture_overhead_blur)
# print(texture_overhead_sharp)
# print(texture_overhead_edge)


y_texture = texture_run_blur[:,0] # 3x3 blurring filter
plt.plot (x_images, y_texture, label = 'Texture Memory')

#######################################################


################# shmem ######################

f3 = open("shMem_Results_3_better.txt", "r")
f9 = open("shMem_Results_9_better.txt", "r")
f11 = open("shMem_Results_11_better.txt", "r")
f15 = open("shMem_Results_15_better.txt", "r")

lines3 = f3.readlines()
lines9 = f9.readlines()
lines11 = f11.readlines()
lines15 = f15.readlines()

n = len(lines)

shmem_run_blur = np.zeros((10,4))
shmem_run_sharp = np.zeros((10,4))
shmem_run_edge = np.zeros((10,1))

shmem_mpixel_blur = np.zeros((10,4))
shmem_mpixel_sharp = np.zeros((10,4))
shmem_mpixel_edge = np.zeros((10,1))

shmem_overhead_blur = np.zeros((10,4))
shmem_overhead_sharp = np.zeros((10,4))
shmem_overhead_edge = np.zeros((10,1))

# for j in range(40):
# 	print(lines9[j])

for image in range(10):
	for ftype in range(3):
		fsize = 0
		i = 2 + ftype*12 + image*39
		# print(lines3[i])
		# print(lines3[i+1])
		ave0 = 0.0
		ave1 = 0.0
		ave2 = 0.0

		l = lines3[i + j + 1]

		for j in range(10):
			# l = lines[i + j + 1]
			t = re.sub(r"\s+", "", l)
			x = t.split(",")
			# print(x[0])
			ave0 = ave0 + float(x[0])
			ave1 = ave1 + float(x[1])
			ave2 = ave2 + float(x[2])
			
		if ftype == 0:
			shmem_run_blur[image, fsize] = ave0/10
			shmem_mpixel_blur[image, fsize] = ave1/10
			shmem_overhead_blur[image, fsize] = ave2/10
		elif ftype == 1:
			shmem_run_sharp[image, fsize] = ave0/10
			shmem_mpixel_sharp[image, fsize] = ave1/10
			shmem_overhead_sharp[image, fsize] = ave2/10
		elif ftype == 2:
			shmem_run_edge[image, fsize] = ave0/10
			shmem_mpixel_edge[image, fsize] = ave1/10
			shmem_overhead_edge[image, fsize] = ave2/10


for image in range(10):
	for ftype in range(2):
		for fsize in range(1,4):
			i = 2 + ftype*12 + image*27
			# print(lines9[i])
			# print(lines9[i+1])

			ave0 = 0.0;
			ave1 = 0.0
			ave2 = 0.0;

			if fsize == 1:
				l = lines9[i + j + 1]
			elif fsize == 2:
				l = lines11[i + j + 1]
			elif fsize == 3:
				l = lines15[i + j + 1]

			for j in range(10):
				# l = lines[i + j + 1]
				t = re.sub(r"\s+", "", l)
				x = t.split(",")
				# print(x[0])
				ave0 = ave0 + float(x[0])
				ave1 = ave1 + float(x[1])
				ave2 = ave2 + float(x[2])
				
			if ftype == 0:
				shmem_run_blur[image, fsize] = ave0/10
				shmem_mpixel_blur[image, fsize] = ave1/10
				shmem_overhead_blur[image, fsize] = ave2/10
			elif ftype == 1:
				shmem_run_sharp[image, fsize] = ave0/10
				shmem_mpixel_sharp[image, fsize] = ave1/10
				shmem_overhead_sharp[image, fsize] = ave2/10


		
# print(shmem_run_blur)
# print(shmem_run_sharp)
# print(shmem_run_edge)

# print(shmem_mpixel_blur)
# print(shmem_mpixel_sharp)
# print(shmem_mpixel_edge)

# print(shmem_overhead_blur)
# print(shmem_overhead_sharp)
# print(shmem_overhead_edge)


y_shmem = shmem_run_blur[:,0] # 3x3 blurring filter
plt.plot (x_images, y_shmem, label = 'Shared Memory')

#######################################################


plt.xlabel('image size')
plt.ylabel('time (ms)')
plt.title('title')

plt.legend()

plt.show()
# plt.savefig('graph.png')

