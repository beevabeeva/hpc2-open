#!/bin/bash

# based on answers from :
# https://stackoverflow.com/questions/8512462/shellscript-looping-through-all-files-in-a-folder
# https://stackoverflow.com/questions/16034749/if-elif-else-statement-issues-in-bash

# loop through images:
for filename in data/Experiment/*; 
# do echo "${filename}"; 
do
	echo "${filename}" >> Constant_Results_.txt
# loop through kernel types (only sharp and blurr)
	for i in {0..2};
	do
		# kernel type
		echo "$i" >> Constant_Results_.txt
# loop through kernel size
		if [ $i -eq 2 ]
		then
# if sobel filter then only do kernel size 3	 
			# kernel size
			echo "3" >> Constant_Results_.txt
	# do it 10 times
			for k in {1..10};
			do
				./constant "${filename}" 3 $i
				# echo "Welcome $i times"
			done
		else
			for j in {3,9,11,15};
			do
				# kernel size
				echo "$j" >> Constant_Results_.txt
	# do it 10 times
				for k in {1..10};
				do
					./constant "${filename}" $j $i
					# echo "Welcome $i times"
				done
			done
		fi
	done
	echo $'\n' >> Constant_Results_.txt
done
